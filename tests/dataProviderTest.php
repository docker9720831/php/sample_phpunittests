<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class dataProviderTest extends TestCase
{
    /**
     * @dataProvider additionProvider
     */
    public function testAdd(int $a, int $b, int $expected): void
    {
        $this->assertSame($expected, $a + $b);
    }

    public function additionProvider(): array
    {
        return [
            'adding zeros'  => [0, 0, 0],
            'zero plus one' => [0, 1, 1],
            'one plus zero' => [1, 0, 1],
            //'one plus one'  => [1, 1, 3]
        ];
    }
/** 
    public function additionProvider(): CsvFileIterator
    {
        return new CsvFileIterator('data.csv');
    }
*/
}
