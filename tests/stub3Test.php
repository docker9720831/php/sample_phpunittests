<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class stub3Test extends TestCase
{
    public function testReturnSelf(): void
    {
        // Create a stub for the SomeClass class.
        $stub = $this->createStub(SomeClass3::class);

        // Configure the stub.
        $stub->method('doSomething')
             ->will($this->returnSelf());

        // $stub->doSomething() returns $stub
        $this->assertSame($stub, $stub->doSomething());
    }
}


class SomeClass3
{
    public function doSomething()
    {
        // Do something.
    }
}

/**
 * The example shown above only works when the original class does not declare a method named “method”.
 * If the original class does declare a method named “method” then 
 * $stub->expects($this->any())->method('doSomething')->willReturn('foo'); 
 * has to be used.
 */