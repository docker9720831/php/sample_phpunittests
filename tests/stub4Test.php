<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;


final class stub4Test extends TestCase
{
    public function testReturnValueMapStub(): void
    {
        // Create a stub for the SomeClass class.
        $stub = $this->createStub(SomeClass4::class);

        // Create a map of arguments to return values.
        $map = [
            ['a', 'b', 'c', 'd'],
            ['e', 'f', 'g', 'h']
        ];

        // Configure the stub.
        $stub->method('doSomething')
             ->will($this->returnValueMap($map));

        // $stub->doSomething() returns different values depending on
        // the provided arguments.
        $this->assertSame('d', $stub->doSomething('a', 'b', 'c'));
        $this->assertSame('h', $stub->doSomething('e', 'f', 'g'));
    }
}


class SomeClass4
{
    public function doSomething()
    {
        // Do something.
    }
}

/**
 * The example shown above only works when the original class does not declare a method named “method”.
 * If the original class does declare a method named “method” then 
 * $stub->expects($this->any())->method('doSomething')->willReturn('foo'); 
 * has to be used.
 */