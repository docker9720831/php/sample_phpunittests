<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class dependencyAndDataProviderComboTest extends TestCase
{
    public function provider(): array
    {
        return [['provider1']];
    }

    public function testProducerFirst(): string
    {
        $this->assertTrue(true);

        return 'first';
    }

    public function testProducerSecond(): string
    {
        $this->assertTrue(true);

        return 'second';
    }

    /**
     * @depends testProducerFirst
     * @depends testProducerSecond
     * @dataProvider provider
     */
    public function testConsumer($a, $b, $c): void
    {
        $this->assertSame(
            ['provider1', 'first', 'second'],
            array($a, $b, $c)
        );
    }
}
