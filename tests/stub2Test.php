<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class stub2Test extends TestCase
{
    public function testStub(): void
    {
        // Create a stub for the SomeClass class.
        $stub = $this->getMockBuilder(SomeClass2::class)
                     ->disableOriginalConstructor()
                     ->disableOriginalClone()
                     ->disableArgumentCloning()
                     ->disallowMockingUnknownTypes()
                     ->getMock();

        // Configure the stub.
        $stub->method('doSomething')
             ->willReturn('foo');

        // Calling $stub->doSomething() will now return
        // 'foo'.
        $this->assertSame('foo', $stub->doSomething());
    }
}

class SomeClass2
{
    public function doSomething()
    {
        // Do something.
    }
}

/**
 * The example shown above only works when the original class does not declare a method named “method”.
 * If the original class does declare a method named “method” then 
 * $stub->expects($this->any())->method('doSomething')->willReturn('foo'); 
 * has to be used.
 */