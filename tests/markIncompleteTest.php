<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class markIncompleteTest extends TestCase
{
    protected function setUp(): void
    {
        // nie działa nie wiem czemu
        if (!extension_loaded('mysqli')) {
            $this->markTestSkipped('The MySQLi extension is not available.');
        }
    }

    public function testSomething(): void
    {
        // działa
        // Optional: Test anything here, if you want.
        $this->assertTrue(true, 'This should already work.');

        // Stop here and mark this test as incomplete.
        //$this->markTestIncomplete('This test has not been implemented yet.');
    }
}