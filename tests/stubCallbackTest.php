<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class stubCallbackTest extends TestCase
{
    public function testReturnCallbackStub(): void
    {
        // Create a stub for the SomeClass class.
        $stub = $this->createStub(SomeClass5::class);

        // Configure the stub.
        $stub->method('doSomething')
             ->will($this->returnCallback('str_rot13'));

        // $stub->doSomething($argument) returns str_rot13($argument)
        $this->assertSame('fbzrguvat', $stub->doSomething('something'));
    }

    public function testOnConsecutiveCallsStub(): void
    {
        // Create a stub for the SomeClass class.
        $stub = $this->createStub(SomeClass5::class);

        // Configure the stub.
        $stub->method('doSomething')
             ->will($this->onConsecutiveCalls(2, 3, 5, 7));

        // $stub->doSomething() returns a different value each time
        $this->assertSame(2, $stub->doSomething());
        $this->assertSame(3, $stub->doSomething());
        $this->assertSame(5, $stub->doSomething());
    }
}

class SomeClass5
{
    public function doSomething()
    {
        // Do something.
    }
}

/**
 * The example shown above only works when the original class does not declare a method named “method”.
 * If the original class does declare a method named “method” then 
 * $stub->expects($this->any())->method('doSomething')->willReturn('foo'); 
 * has to be used.
 */