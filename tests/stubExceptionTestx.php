<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class stubExceptionTest extends TestCase
{
    public function testThrowExceptionStub(): void
    {
        // Create a stub for the SomeClass class.
        $stub = $this->createStub(SomeClass6::class);

        // Configure the stub.
        $stub->method('doSomething')
             ->will($this->throwException(new Exception));

        // $stub->doSomething() throws Exception
        //$stub->doSomething();
    }
}

class SomeClass6
{
    public function doSomething()
    {
        // Do something.
    }
}

/**
 * The example shown above only works when the original class does not declare a method named “method”.
 * If the original class does declare a method named “method” then 
 * $stub->expects($this->any())->method('doSomething')->willReturn('foo'); 
 * has to be used.
 */